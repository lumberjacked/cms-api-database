<?php

return array(

     'factories' => array(
         'Cms\Api\Database\Extension\DbManager'  => 'Cms\Api\Database\Extension\DbManagerFactory',
         'Cms\Api\Database\Extension\SchemaTool' => 'Cms\Api\Database\Extension\SchemaToolFactory',
         'hydrator'    => 'Cms\Api\Database\Hydrator\HydratorFactory'
     ),

     'aliases' => array(
            'Cms\Api\Database\Hydrator\Hydrator' => 'hydrator'
        ),

     
);