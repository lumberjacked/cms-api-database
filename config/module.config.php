<?php

return array(

    'doctrine' => array(
        
        'connection' => array(

            'sqlite' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOSqlite\Driver',
                'params' => array(
                    'path'     => getcwd() . '/data/cms.db'
                )
            ),
        ),

        'driver' => array(
            'application_entities' => array(
              'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
              'cache' => 'array',
              'paths' => array(__DIR__ . '/../src/Entity')
            ),

            'orm_default' => array(
              'drivers' => array(
                'Cms\Api\Database\Entity' => 'application_entities'
              )
            ),

            'sqlite' => array(
              'drivers' => array(
                'Cms\Api\Database\Entity' => 'application_entities'
              )
            )
        ),

    ),


    'bas_cms' => array(
        'extensions' => array(
            'dbmanager' => array(
                'type'    => 'Cms\Api\Database\Extension\DbManager',
                'options' => array(
                    'initializer' => true,
                    'listeners' => array(
                        'db.event' => 'dbEvent'
                    )
                )
            ),
            'schema-tool' => array(
                'type' => 'Cms\Api\Database\Extension\SchemaTool',
                'options' => array(
                    'initializer' => true,
                    'listeners' => array(
                        'cms.schema.create' => 'create',
                    )
                )
            )
        ), 

    ),
);