<?php
namespace Cms\Api\Database\Hydrator;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HydratorFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        
        $xmanager = $serviceLocator->get('Cms\ExtensionManager\Extension\XmanagerExtension');
        $em       = $xmanager->getExtension('dbmanager')->getEntityManager();
        return new \DoctrineModule\Stdlib\Hydrator\DoctrineObject($em);
    }
}