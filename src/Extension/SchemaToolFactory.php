<?php
namespace Cms\Api\Database\Extension;

use Zend\ServiceManager\ServiceLocatorInterface;

class SchemaToolFactory {

    public function __invoke(ServiceLocatorInterface $services) {
        
        return new SchemaTool();
    }
}