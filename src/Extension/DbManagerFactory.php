<?php
namespace Cms\Api\Database\Extension;

use Zend\ServiceManager\ServiceLocatorInterface;

class DbManagerFactory {

    public function __invoke(ServiceLocatorInterface $services) {
        
        return new DbManager();
    }
}