<?php
namespace Cms\Api\Database\Extension;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool as DoctrineSchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Extension\AbstractExtension;

class SchemaTool extends AbstractExtension {

    protected $em;

    public function create(ResponderEvent $e) {
        
        try{ 
            $tool = $this->getSchemaTool();
            $tool->createSchema($this->getEntityClasses());

        } catch(ToolsException $exception) {
            
            return $e->responder(null, true, $exception->getMessage(), array(), 500);
        }
        
        return $e->responder(null, false, 'Database Schema Created Successfully');
    }

    protected function getSchemaTool() {
        return new DoctrineSchemaTool($this->getEntityManager()); 
    }

    protected function getEntityManager() {
        
        if(null == $this->em) {
            $this->em = $this->get('dbmanager')->getEntityManager();
        }
        return $this->em; 
    } 

    protected function getEntityClasses() {
        
        $em       = $this->getEntityManager();
        $config   = $this->trigger('get.cms.config');
        $paths    = $config->getDoctrinePaths();
        
        $classes = array();

        foreach($paths as $index => $path) {
            
            $files = array_diff(scandir($path), array('..', '.'));                
            
                foreach($files as $i => $class) {
                    
                    $class = str_replace('.php', '', lcfirst($class));
                    
                    if($class != '.gitignore') {
                        $class = $this->get($class);
                        $classes[] = $em->getClassMetadata(get_class($class));   
                    }
                }
        }

        return $classes;
    }

}