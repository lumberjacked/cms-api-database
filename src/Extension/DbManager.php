<?php
namespace Cms\Api\Database\Extension;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Cms\ExtensionManager\Extension\DbRequester;
use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Extension\AbstractExtension;

class DbManager extends AbstractExtension {

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;

        return $this->em;
    }
 
    public function getEntityManager() {
        
        if (null === $this->em) {
            
            try {
                $em = $this->get('doctrine.entitymanager.orm_default');
                $em->getConnection()->connect();
                $em->getConnection()->close();

                $this->setEntityManager($em);   
            } catch(\Exception $e) {
                $em = $this->createEntityManager();

                try {
                    $em->getConnection()->connect();
                    $em->getConnection()->close();

                    return $em;   
                } catch(\Exception $e) {
                    var_dump($e->getMessage());die();
                }
                
            }    
        }

        return $this->em;
    }

    protected function createEntityManager() {

        $config = $this->trigger('get.cms.config');
        
        $paths = $config->getDoctrinePaths();
               
        $dbParams = $config->getDoctrineParams();
        
        $config = Setup::createAnnotationMetadataConfiguration($paths, true, null, null, false);

        return EntityManager::create($dbParams, $config);

    }

    public function dbEvent(ResponderEvent $e) {
        
        if(!($e->getParams() instanceof DbRequester)) {
            
            return $e->responder(
                    $e->getName(),
                    true, 
                    sprintf("Database calls require the use of Cms/ExtensionManager/Extension/DbRequester -- %s given instead", 
                         gettype($e->getParams())
                    ),
                    null,
                    500
            );
        }
        
        $requester = $e->getParams();
        
        $entities = $this->getResource($requester->getResource(), $requester->getMethod(), $requester->getParams());
        if($entities instanceof \Exception) {
            return $e->responder($requester->getResponseMethod(), true, $entities->getMessage(), $requester->getParams(), 500);    
        }

        return $e->responder($requester->getResponseMethod(), 
                             false, 
                             sprintf('Entities found by %s', $requester->getResponseMethod()),
                             array($requester->getresource() => $entities)
                );

    }

    protected function getResource($resource, $method, array $params = array()) {
        
        $try_method = $this->exchangeMethod($method);
        
        if(method_exists($this->getRepo($resource), $try_method)) {
            
            try {
                $repo     = $this->getRepo($resource);
                $entities = $repo->$try_method($params);

                if(null == $entities) {
                    return new \Exception(sprintf('Entity %s with id{%s} said --> method{%s} returned void;', $resource, $params['id'], $method), 424); 
                
                }

                return $this->hydrate($entities);
            } catch(\Exception $e) {
                return new \Exception(sprintf('Entity %s with id{%s} said --> %s', $resource, $params['id'], $e->getMessage()), 503);   
            }


        } else {
            return new \Exception(sprintf('Entity %s said --> method{%s} does not exist;', $resource, $method), 503);    
        }
    }

    protected function getRepo($resource) {
        $entity = $this->get($resource);
        $entity = get_class($entity);
        $repo   = $this->getEntityManager()->getRepository($entity);
        
        return $repo;
    }

    protected function exchangeMethod($exchange) {

        $exchange = explode('.', $exchange);
        
        if(count($exchange) > 1) {
            foreach($exchange as $index => $value) {
                if($index == 0) {
                    $method = $value;
                } else {
                    $method = $method . ucfirst($value);
                }
            }    
        } else {
            $method = $exchange[0];
        }

        

        return $method;
    }

    protected function hydrate($entities) {
        $hydrator = $this->get('hydrator'); 

        if(is_object($entities)) {
            $entities = $hydrator->extract($entities);
        
        } else if(is_array($entities)) {
            foreach($entities as $index => $entity) {
                $entities[$index] = $hydrator->extract($entity);
            }

            if(count($entities) == 1) {
                $entities = $entities[0];
            }
        } else {
            return new \Exception(sprintf('Database entity hydration said --> entities{%s} must either be entity object or array of entity objects;', gettype($entities)), 503);
        } 

        return $entities; 
    }

}